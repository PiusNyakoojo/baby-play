import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import PlayKeyboardCar from './views/PlayKeyboardCar.vue'
import PlayKeyboardAnimals from './views/PlayKeyboardAnimals.vue'
import PlayKeyboardNumbers from './views/PlayKeyboardNumbers.vue'


Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/play-keyboard-car', name: 'play-keyboard-car', component: PlayKeyboardCar },
    { path: '/play-keyboard-animals', name: 'play-keyboard-animals', component: PlayKeyboardAnimals },
    { path: '/play-keyboard-numbers', name: 'play-keyboard-numbers', component: PlayKeyboardNumbers }
  ]
})
