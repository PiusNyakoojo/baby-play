# baby-play

## Features
- Play with a car using the keyboard: Press any key to move the car in a random direction
- Play with animals using the keyboard: Press any key to show an animal image and play an animal sound
- Play with numbers using the keyboard: Press any key to show the number of apples

## Inspiration
My 1.5 year old baby brother is smart and always hungry to learn. However, most of the existing
online toddler games are overwhelming with the requirement to use specific keys or precise mouse
movements that aren't accommodating to a baby's large arm waving movements and random smashing
of the keys on a keyboard.

It would be useful to have a tool that accommodates a baby's playfulness and progressively enables
more features as the baby learns to choose specific keys and to maneuver the mouse towards an
intended result.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
